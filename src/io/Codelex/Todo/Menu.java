package io.Codelex.Todo;

import java.util.Scanner;


public class Menu {


    public static void showMenu(){
        TaskFile file = new TaskFile();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose one of the following options:");
        System.out.println("1. Add task");
        System.out.println("2. Remove task");
        System.out.println("3. Update task");
        System.out.println("4. Mark as checked");
        System.out.println("5. View tasks");
        System.out.println("6. Exit");


        int n = scanner.nextInt();
        switch (n) {
            case 1:
                file.addTask();
                break;
            case 2:
                file.removeTask();
                break;
            case 3:
                file.updateTask();
                break;
            case 4:
                file.markAsChecked();
                break;
            case 5:
                file.viewUncompletedTasks();
                file.viewCompletedTasks();
                break;
            case 6:
                System.exit(0);
                break;
            default:
                System.err.println("Choose from 1 to 6!");
        }
        showMenu();
    }

}
