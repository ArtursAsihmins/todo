package io.Codelex.Todo;

import java.util.Scanner;

import static io.Codelex.Todo.TaskList.tasks;
import static io.Codelex.Todo.TaskList.completed;

public class TaskFile {

    public void addTask() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Add task : ");
        String title = scan.nextLine();
        Task element = new Task();
        element.setTitle(title);
        tasks.add(element);


        System.out.println("Task added: " + element);
        System.out.println("***********");
    }


    public void removeTask() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Remove task: ");
        String removeTitle = scan.nextLine();

        for (Task element : tasks) {
            if (element.getTitle().equals(removeTitle)) {
                tasks.remove(element);

                System.out.println("Task removed!");
                System.out.println("***********");
            } else {
                System.err.println("Task not found!");
                System.out.println("***********");
            }
            break;
        }
    }


    public  void updateTask() {
        Scanner update = new Scanner(System.in);
        Scanner task = new Scanner(System.in);
        boolean updated = false;

        System.out.print("Update task: ");
        String updateTitle = update.nextLine();

        for (Task newTask : tasks) {
            if (newTask.getTitle().equals(updateTitle)) {
                System.out.print("New task: ");
                String newTitle = task.nextLine();
                newTask.setTitle(newTitle);
                updated = true;
            }
            if (updated) {
                System.out.println("Task updated!");
                System.out.println("***********");
            } else {
                System.err.println("Task not found!");
                System.out.println("***********");
            }
            break;
        }
    }


    public  void markAsChecked() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Mark task as checked: ");
        String markAsCheckedTitle = scan.nextLine();

        for (Task element : tasks) {
            if (element.getTitle().equals(markAsCheckedTitle)) {
                tasks.remove(element);
                System.out.println("Task completed!");
                System.out.println("***********");
                completed.add(element);
            } else {
                System.err.println("Task not found!");
                System.out.println("***********");
            }
            break;
        }
    }


    public  void viewUncompletedTasks () {
        System.out.println("Uncompleted task(s): ");
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println((i + 1) + ". " + tasks.get(i));
        }
    }


    public void viewCompletedTasks () {
        System.out.println("Completed task(s):");
        for (int i = 0; i < completed.size(); i++) {
            System.out.println((i + 1) + ". " + completed.get(i));
        }
    }
}
